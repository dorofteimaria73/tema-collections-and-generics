import java.util.HashSet;
import java.util.Random;
import java.util.Set;


public class RandomNumbers {
    public static void main(String[] args) {
        Set<Integer> generated = new HashSet<>();
        Random r = new Random();
        while (generated.size() < 10) {
            int num = r.nextInt(20) + 1;
            generated.add(num);
        }
        System.out.println(generated);
    }
}
